FactoryBot.define do
  factory :post do
    title    { "#{Faker::TvShows::RickAndMorty.character} at #{Faker::TvShows::RickAndMorty.location}" }
    content  { Faker::TvShows::RickAndMorty.quote[0..255] }

    user
  end
end
