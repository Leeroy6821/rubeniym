FactoryBot.define do
  factory :comment do
    content  { Faker::Movies::Lebowski.quote[0..255] }

    user
    post
  end
end
