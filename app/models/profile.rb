# frozen_string_literal: true

class Profile < ApplicationRecord
  belongs_to :user

  validates :first_name, presence: true
  validates :first_name, length: { in: 2..20 }

  validates :last_name, presence: true
  validates :last_name, length: { in: 2..20 }

  validates :age, presence: true
  validates :age, numericality: { only_integer: true }

  validates :bio, length: { in: 5..255 }, allow_nil: true
end
