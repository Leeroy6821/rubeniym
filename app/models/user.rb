# frozen_string_literal: true

class User < ApplicationRecord
  has_one :profile
  has_many :posts
  has_many :comments

  validates :email, presence: true
  validates :email, uniqueness: true

  validates :password, presence: true
  validates :password, length: { minimum: 10 }
end
